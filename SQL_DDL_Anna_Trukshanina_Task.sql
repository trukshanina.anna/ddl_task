/* ссылка на схему
https://drawsql.app/teams/anna-41/diagrams/copy-of-copy-of-mountaineering-club
*/

create table Certification(
    certification_id serial primary key not null,
    certification_name text not null
    check (Certification_name is not null and certification_name != '')
); 

create table ClimberInsurance(
    insurance_id serial primary key not null,
    climber_id int not null,
    insurance_company text not null,
    policy_number int not null,
	unique (insurance_id, policy_number),
    CoverageStartDate date not null,
    CoverageEndDate date not null,
    check (policy_number >= 0)
);

create table Country(
    country_id serial primary key not null,
    country_name text not null
);

create table City(
    city_id serial primary key not null,
    city_name text not null,
    country_id int not null,
    state text not null,
    foreign key(country_id) references Country(country_id)
);

create table Adresses(
    adress_id serial primary key not null,
    city_id int not null,
    sreet text not null,
    postcode text not null,
    foreign key(city_id) references City(city_id)
);

create table MountainArea (
    area_id serial primary key not null,
    area_name text not null
); 

create table MountainAreaCountries (
    area_id int,
    country_id int,
    primary key (area_id, country_id),
    foreign key (area_id) references MountainArea(area_id),
    foreign key (country_id) references Country(country_id)
);

create table Mountain(
    mountain_id serial primary key not null,
    mountain_name text not null,
    mountain_height int not null,
    country_id int not null,
	area_id int not null,
	foreign key (area_id) references MountainArea(area_id),
    foreign key(country_id) references Country(country_id)
);

create table Climber(
    climber_id serial primary key not null,
    climber_name text not null,
    adress_id int not null,
    insurance_id int not null,
    gender text not null check (gender in ('Male', 'Female', 'Other')),
    unique (climber_name, gender),
    foreign key(insurance_id) references ClimberInsurance(insurance_id),
    foreign key(adress_id) references Adresses(adress_id)
);

create table Climbs(
    climb_id serial primary key not null,
    climber_id int not null,
    mountain_id int not null,
    start_date date not null check (start_date > '2000-01-01'),
    end_date date not null check (end_date > '2000-01-01'),
    unique (climber_id, mountain_id, start_date, end_date),
    foreign key(climber_id) references Climber(climber_id),
    foreign key(mountain_id) references Mountain(mountain_id)
);

create table ClimberCertification (
    climber_id int,
    certification_id int,
	certification_date date,
    PRIMARY KEY (climber_id, certification_id),
    foreign key (climber_id) references Climber(climber_id),
    foreign key (certification_id) references Certification(certification_id)
);

INSERT INTO Certification (certification_name) VALUES
('Rock Climbing Level 1'),
('Ice Climbing Certification'),
('Mountain Rescue Training');

INSERT INTO ClimberInsurance (climber_id, insurance_company, policy_number, CoverageStartDate, CoverageEndDate) VALUES
(1, 'InsuranceCo A', 12345, '2023-01-01', '2024-01-01'),
(2, 'InsuranceCo B', 67890, '2023-02-01', '2024-02-01');

INSERT INTO Country (country_name) VALUES
('India'),
('Nepal'),
('USA'),
('Canada'),
('France'),
('Italy');

INSERT INTO City (city_name, country_id, state) VALUES
('New York', 1, 'NY'),
('Toronto', 2, 'ON');

INSERT INTO Adresses (city_id, sreet, postcode) VALUES
(1, 'Broadway St', '10001'),
(2, 'King St', 'M5V 1M2');

INSERT INTO MountainArea (area_name) VALUES
    ('Himalayas'),
    ('Rocky Mountains'),
    ('Alps');
	
INSERT INTO MountainAreaCountries (area_id, country_id) VALUES
    (1, 1), -- Himalayas, India
    (1, 2), -- Himalayas, Nepal
    (2, 3), -- Rocky Mountains, USA
    (2, 4), -- Rocky Mountains, Canada
    (3, 5), -- Alps, France
    (3, 6); -- Alps, Italy

INSERT INTO Mountain (mountain_name, mountain_height, area_id, country_id) VALUES
    ( 'Mount Everest', 8848, 1, 1), -- Himalayas, India
    ( 'Kangchenjunga', 8586, 1, 2), -- Himalayas, Nepal
    ( 'Denali', 6190, 2, 3), -- Rocky Mountains, USA
    ( 'Mount Robson', 3954, 2, 4), -- Rocky Mountains, Canada
    ( 'Mont Blanc', 4808, 3, 5), -- Alps, France
    ( 'Matterhorn', 4478, 3, 6); -- Alps, Italy

INSERT INTO Climber (climber_name, adress_id, insurance_id, gender) VALUES
('John Doe', 1, 1, 'Male'),
('Jane Smith', 2, 2, 'Female');

INSERT INTO Climbs (climber_id, mountain_id, start_date, end_date) VALUES
(1, 1, '2023-03-01', '2023-03-10'),
(2, 2, '2023-04-01', '2023-04-15');

INSERT INTO ClimberCertification (climber_id, certification_id, certification_date)
VALUES
    (1, 1, '2023-01-15'), -- John Doe received 'Rock Climbing Level 1' certification on January 15, 2023
    (1, 3, '2023-02-20'), -- John Doe received 'Mountain Rescue Training' certification on February 20, 2023
    (2, 2, '2023-03-10'), -- Jane Smith received 'Ice Climbing Certification' on March 10, 2023
    (2, 1, '2023-04-05'); -- Jane Smith received 'Rock Climbing Level 1' certification on April 5, 2023
	
-- Add 'record_ts' field to Certification table
ALTER TABLE Certification
ADD COLUMN record_ts date DEFAULT current_date;

SELECT * FROM Certification;

-- Add 'record_ts' field to ClimberInsurance table
ALTER TABLE ClimberInsurance
ADD COLUMN record_ts date DEFAULT current_date;

SELECT * FROM ClimberInsurance;

-- Add 'record_ts' field to MountainEvents table
ALTER TABLE MountainAreaCountries 
ADD COLUMN record_ts date DEFAULT current_date;

SELECT * FROM MountainAreaCountries;

-- Add 'record_ts' field to Country table
ALTER TABLE Country
ADD COLUMN record_ts date DEFAULT current_date;

SELECT * FROM Country;

-- Add 'record_ts' field to City table
ALTER TABLE City
ADD COLUMN record_ts date DEFAULT current_date;

SELECT * FROM City;

-- Add 'record_ts' field to Adresses table
ALTER TABLE Adresses
ADD COLUMN record_ts date DEFAULT current_date;

SELECT * FROM Adresses;

-- Add 'record_ts' field to Adresses table
ALTER TABLE MountainArea
ADD COLUMN record_ts date DEFAULT current_date;

SELECT * FROM MountainArea;

-- Add 'record_ts' field to Mountains table
ALTER TABLE Mountain
ADD COLUMN record_ts date DEFAULT current_date;

SELECT * FROM Mountain;

-- Add 'record_ts' field to Climbers table
ALTER TABLE Climber
ADD COLUMN record_ts date DEFAULT current_date;

SELECT * FROM Climber;

-- Add 'record_ts' field to Climbs table
ALTER TABLE Climbs
ADD COLUMN record_ts date DEFAULT current_date;

SELECT * FROM Climbs;

-- Add 'record_ts' field to  ClimberCertification  table
ALTER TABLE  ClimberCertification 
ADD COLUMN record_ts date DEFAULT current_date;

SELECT * FROM  ClimberCertification;
